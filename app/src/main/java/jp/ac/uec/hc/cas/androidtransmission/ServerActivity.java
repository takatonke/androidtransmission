package jp.ac.uec.hc.cas.androidtransmission;

import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

public class ServerActivity extends AppCompatActivity {
    TransmissionServer transmissionServer;
    TransmissionServer transmissionServer2;
    Handler mHandler;
    TextView serverReceivedView;
    ScrollView serverScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        getIntent();

        mHandler = new Handler();
        serverReceivedView = (TextView) findViewById(R.id.serverReceivedView);
        serverScrollView = (ScrollView) findViewById(R.id.serverScrollView);

        final EditText portText = (EditText) findViewById(R.id.clientPort);
        final EditText port2Text = (EditText) findViewById(R.id.client2Port);
        final Button serverStartButton = (Button) findViewById(R.id.serverStartButton);
        serverStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transmissionServer = new TransmissionServer(Integer.parseInt(port2Text.getText().toString()));
                transmissionServer.addObserver(observer);
                transmissionServer.start();
                transmissionServer2 = new TransmissionServer(Integer.parseInt(portText.getText().toString()));
                transmissionServer2.addObserver(observer);
                transmissionServer2.start();
                if(serverStartButton.getText() == getString(R.string.start))
                    serverStartButton.setText(getString(R.string.stop));
                else
                    serverStartButton.setText(getText(R.string.start));
            }
        });

        final EditText serverSendCommand = (EditText) findViewById(R.id.serverSendCommand);
        Button serverSendButton = (Button) findViewById(R.id.serverSendButton);
        serverSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transmissionServer.send("message", serverSendCommand.getText().toString());
                transmissionServer2.send("message", serverSendCommand.getText().toString());
            }
        });
    }

    private Observer observer = new Observer() {
        @Override
        public void updateMessage(String message) {
            final String receivedMessage = message;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    serverReceivedView.setText(serverReceivedView.getText().toString() + receivedMessage + "\n");
                    serverScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(transmissionServer != null) {
            transmissionServer.removeObserver(observer);
        }
        if(transmissionServer2 != null) {
            transmissionServer2.removeObserver(observer);
        }
    }
}
