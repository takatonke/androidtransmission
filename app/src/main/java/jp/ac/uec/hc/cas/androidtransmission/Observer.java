package jp.ac.uec.hc.cas.androidtransmission;

/**
 * Created by Takato on 2016/07/27.
 */
public interface Observer {
    void updateMessage(String message);
}
