package jp.ac.uec.hc.cas.androidtransmission;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

public class ClientActivity extends AppCompatActivity {
    EditText serverIp;
    EditText serverPort;

    TransmissionClient transmissionClient;

    Handler mHandler;
    TextView clientReceivedView;
    ScrollView clientScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        getIntent();

        mHandler = new Handler();
        clientReceivedView = (TextView) findViewById(R.id.clientReceivedView);
        clientScrollView = (ScrollView) findViewById(R.id.clientScrollView);

        serverIp = (EditText) findViewById(R.id.serverIP);
        serverPort = (EditText) findViewById(R.id.serverPort);

        final Button clientStartButton = (Button) findViewById(R.id.clientStartButton);
        clientStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transmissionClient = new TransmissionClient(serverIp.getText().toString(), Integer.parseInt(serverPort.getText().toString()));
                transmissionClient.addObserver(observer);
                transmissionClient.start();
                if(clientStartButton.getText() == getString(R.string.start))
                    clientStartButton.setText(getString(R.string.stop));
                else
                    clientStartButton.setText(getString(R.string.start));
            }
        });

        final EditText clientSendCommand = (EditText) findViewById(R.id.clientSendCommand);
        Button clientSendButton = (Button) findViewById(R.id.clientSendButton);
        clientSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transmissionClient.send("message", clientSendCommand.getText().toString());
            }
        });
    }

    private Observer observer = new Observer() {
        @Override
        public void updateMessage(String message) {
            final String receivedMessage = message;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    clientReceivedView.setText(clientReceivedView.getText().toString() + receivedMessage + "\n");
                    clientScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(transmissionClient != null) {
            transmissionClient.removeObserver(observer);
        }
    }
}
