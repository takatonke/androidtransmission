package jp.ac.uec.hc.cas.androidtransmission;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Takato on 2016/07/27.
 */
public class TransmissionServer extends Thread {
    ServerSocket serverSocket = null;
    Socket socket = null;
    BufferedReader in = null;
    BufferedWriter out = null;

    private int port;

    private boolean waiting;
    private boolean sendStatus;
    private boolean c = true;

    private String message;
    ArrayList<ServerSnedTime> sendTimeArrayList = new ArrayList<ServerSnedTime>();

    private List<Observer> observers = new CopyOnWriteArrayList<Observer>();

    public TransmissionServer(int portNumber) {
        port = portNumber;
        waiting = true;
        sendStatus = false;
    }

    public void send(String tag, String message) {
        boolean tmpMatch = false;

        for(int i = 0; i < sendTimeArrayList.size(); i++) {
            if(tag == sendTimeArrayList.get(i).tag) {
                if(!sendTimeArrayList.get(i).toGivePermissionSend()) {
                    return;
                }
                tmpMatch = true;
                break;
            }
        }

        if(!tmpMatch) {
            sendTimeArrayList.add(new ServerSnedTime(tag));
            sendStatus = true;
        }
        this.message = message;
        sendStatus = true;
    }

    @Override
    public void run() {
        long sendTime = System.currentTimeMillis();
        while (c) {
            try {
                serverSocket = new ServerSocket(port);
                socket = serverSocket.accept();

                out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while (waiting) {
                    if(in.ready()) {
                        String str = in.readLine();
                        if(!str.equals("ping"))
                            notifyObservers(str);
                    }

                    long now = System.currentTimeMillis();
                    if (sendStatus) {
                        sendStatus = false;
                        out.write(message);
                        out.newLine();
                        out.flush();
                        sendTime = now;
                    } else if (now - sendTime > 1000) {
                        out.write("ping");
                        out.newLine();
                        out.flush();
                        sendTime = now;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (serverSocket != null) {
                        serverSocket.close();
                    }
                    if (socket != null) {
                        socket.close();
                    }
                    if (in != null) {
                        in.close();
                    }
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(10000);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void notifyObservers(String message) {
        for(Observer observer: observers)
            observer.updateMessage(message);
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
        if(observers.isEmpty()) {
            destroyTransmission();
        }
    }

    private void destroyTransmission() {
        waiting = false;
        c = false;
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
            if (socket != null) {
                socket.close();
            }
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class ServerSnedTime {
    long sentTimeMillis;
    String tag;

    public ServerSnedTime(String tag) {
        this.tag = tag;
        sentTimeMillis = System.currentTimeMillis();
    }

    public boolean toGivePermissionSend() {
        long now = System.currentTimeMillis();
        if(now - this.sentTimeMillis > 100) {
            this.sentTimeMillis = now;
            return true;
        } else {
            return false;
        }
    }
}